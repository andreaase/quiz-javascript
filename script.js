alert("Quiz da GTI! :)") //para aparecer uma caixa de texto ao carregar a página com a mensagem "Bem vindo ao Quiz da GTI"

var questions
var httpRequest = new XMLHttpRequest(); /*função AJAX XMLHttpRequest que transfere dados entre um cliente e um servidor
continua nas 2 linhas seguintes com a função open e send*/

httpRequest.open('GET', 'https://quiz-trainee.herokuapp.com/questions', true); //pega as questões da API
httpRequest.send();

httpRequest.onreadystatechange = function(){ /* onreadystatechange é a propriedade AJAX que define a função que deve 
    ser executada quando o readyState e o status mudar para os valores 4 e 200*/
   if(this.readyState == 4 && this.status == 200){/*o valor 4  do readyState significa que a requisição está 
    finalizada e a resposta está pronta e o valor 200 significa a mensagem "OK"*/
      questions = (JSON.parse(this.responseText));/*função JSON parse() retorna o objeto javascript 
      da propriedade responseText do XMLHttpRequest, fazendo com que a variável questions corresponda
      às questões trazidas da API*/
   }
}

var pontuacao = 0;//valores iniciais atribuídos a pontuação e a quest
var quest = -1

function mostrarQuestao() {//Sempre que o botão (id: confirmar) for clicado essa função será executada
    console.log(questions); //carrega as questões no console
    if (quest == -1) {/*Se o valor de quest for igual a -1 é porque é a primeira vez que a função mostrarQuestao
        é executada (pois sempre ao fim da função há um quest++)*/
        document.getElementById("resultado").style.display = 'none'//resultado não deverá aparecer
        document.getElementById("listaRespostas").style.display = "inline"//"listaRespostas" aparece
        document.getElementById("confirmar").innerHTML = "Próxima"//muda a aparência do texto do botão de "Começar" para "Próxima"
    } 

    quest++//aumenta o valor de quest em +1

    if (quest >= questions.length) {/*se o valor de quest for maior ou igual ao número de questões da string do JSON
        executa-se a função finalizarQuiz que irá mostrar a pontuação feita e reiniciará os valores das variáveis*/
        finalizarQuiz();
        return;
    }
    
    document.getElementById('titulo').innerHTML = questions[quest].title/*de acordo com o valor de quest é mudado
    o título da pergunta, pelo valor do índice da string, que no caso, é de 0 a 5*/

    var resp1 = document.getElementsByTagName("span")
    var botresp = document.getElementsByName("resposta")//botão da resposta

    for (var i = 0; i < questions[quest].options.length; i++) {/*essa função irá somar os pontos feitos no quiz, além de 
        atualizar os textos das respostas e seus valores*/
        if (botresp[i].checked) {/*quando se chegar ao botão (valor i) que está selecionado adiciona-se seu valor
            à pontuação*/
            pontuacao += Number(botresp[i].value)//pontuacao=pontuacao + valor da resposta correspondente ao botão selecionado
            botresp[i].checked = false // tira a seleção do botão para que ele não continue selecionado na próxima pergunta
        }
        resp1[i].innerHTML = questions[quest].options[i].answer//faz a atualização dos textos das respostas
        resp1[i].parentElement.children[0].value = questions[quest].options[i].value//faz a atualização dos valores das respostas

    }

}

function finalizarQuiz() {
    var porcentagemFinal = Math.round((pontuacao / (questions.length*3)) * 100)/*o valor máximo atribuido a uma
    questão é 3, portando faz-se a divisão do valor total de pontos feitos no quiz pela quantidade de questõas
    multiplicada por 3, dividido por 100 para que o valor seja dado em porcentagem*/
    quest = -1 //reinicia-se os valores das variáveis
    pontuacao = 0
    document.getElementById('titulo').innerHTML = 'QUIZ DOS VALORES DA GTI'//muda novamente o título
    document.getElementById("listaRespostas").style.display = "none"//faz que nenhuma opção de resposta apareça, já que se está no final do quiz
    document.getElementById("resultado").style.display = 'block'//faz que resultado apareça, na função mostrarQuestao seu valor estava em none
    document.getElementById("resultado").innerHTML = 'Sua pontuação: ' + porcentagemFinal + '%'
    document.getElementById("confirmar").innerHTML = "Refazer Quiz"; //muda a aparência do texto do botão para "Refazer Quiz"
}